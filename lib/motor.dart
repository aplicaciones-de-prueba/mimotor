import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    // ignore: prefer_const_constructors
    return MaterialApp(
      title: 'Mimotor',
      home: const Mimotor(),
    );
  }
}

class Mimotor extends StatelessWidget {
  const Mimotor({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'Mimotor',
        home: Scaffold(
          appBar: AppBar(
            title: const Text("Mimotor"),
            backgroundColor: const Color.fromARGB(255, 255, 115, 0),
          ),
          body: const Text("Mimotor"),
        ));
  }
}
